package com.bmx.desafioconcrete.data.room

import android.arch.persistence.room.*

/**
 * Created by thiagopassos on 12/12/2017.
 */
@Entity
data class User(
        @PrimaryKey @ColumnInfo(name = "username") var username: String = "",
        @ColumnInfo(name = "name") var name: String = "",
        @ColumnInfo(name = "gravatar_url") var gravatarUrl: String = ""
)

@Entity(foreignKeys = arrayOf(ForeignKey(entity = User::class,
        parentColumns = arrayOf("username"),
        childColumns = arrayOf("owner"))),
        indices = arrayOf(Index(value = "owner", name = "owner"))
)
data class Repository(
        @PrimaryKey var id: Long = 0,
        var owner: String = "",
        var name: String = "",
        @ColumnInfo(name = "full_name") var fullName: String = "",
        var description: String = "",
        @ColumnInfo(name = "forks_count") var forksCount: Int = 0,
        @ColumnInfo(name = "stargazers_count") var stargazersCount: Int = 0
)

@Entity(foreignKeys = arrayOf(ForeignKey(entity = User::class,
        parentColumns = arrayOf("username"),
        childColumns = arrayOf("assignee"))),
        indices = arrayOf(Index(value = "assignee", name = "assignee"))
)
data class PullRequest(
        @PrimaryKey var id: Long = 0,
        var assignee: String = "",
        var url: String = "",
        var state: String = "",
        var title: String = "",
        var body: String = ""
)