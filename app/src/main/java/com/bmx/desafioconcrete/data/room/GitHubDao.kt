package com.bmx.desafioconcrete.data.room

import android.arch.persistence.room.*

/**
 * Created by thiagopassos on 12/12/2017.
 */
@Dao
interface GitHubDao {
    @Query("SELECT * FROM user WHERE username = :user")
    fun getUser(user: String) : User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(users: Array<User>)

    @Delete()
    fun deleteUser(users: Array<User>)

    @Update()
    fun updateUser(users: Array<User>)

    @Query("SELECT * FROM repository")
    fun getRepositories() : List<Repository>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepository(repositories: Array<Repository>)

    @Delete()
    fun deleteRepository(repositories: Array<Repository>)

    @Update()
    fun updateRepository(repositories: Array<Repository>)

    @Query("SELECT * FROM pullrequest")
    fun getPullRequests() : List<PullRequest>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPullRequest(pullRequests: Array<PullRequest>)

    @Delete()
    fun deletePullRequest(pullRequests: Array<PullRequest>)

    @Update()
    fun updatePullRequest(pullRequests: Array<PullRequest>)
}