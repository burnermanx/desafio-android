package com.bmx.desafioconcrete.data.service

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Callback
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import timber.log.Timber

/**
 * Created by thiagopassos on 12/12/2017.
 */
class GitHubService {
    companion object Instance {
        private val httpLogging = HttpLoggingInterceptor(
                HttpLoggingInterceptor.Logger { Timber.tag("OkHttp").d(it) }
        )

        private val client = OkHttpClient.Builder()
                .addInterceptor(httpLogging)
                .build()

        private val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .baseUrl("https://api.github.com/")
                .build()

        val service = retrofit.create(GitHubApi::class.java)
    }
}

interface GitHubApi {
    @Headers("Accept: application/vnd.github.v3+json")
    @GET("/search/repositories?q=language:Java&sort=stars&page={page}")
    fun getRepositories(@Path("page") page: Int,
                        callback: Callback<List<Repository>>)

    @Headers("Accept: application/vnd.github.v3+json")
    @GET("/repos/{fullName}/pulls")
    fun getPullRequests(@Path("fullName") fullName: String,
                        callback: Callback<List<PullRequest>>)

    @Headers("Accept: application/vnd.github.v3+json")
    @GET("/users/{user}")
    fun getUser(@Path("user") username: String,
                callback: Callback<GitHubUser>)
}

data class GitHubUser(val id: Long,
                      val login: String,
                      val name: String,
                      val avatar_url: String)

data class RelatedUser(val id: Long,
                       val login: String)

data class Repository(val id: Long,
                      val owner: RelatedUser,
                      val name: String,
                      val full_name: String,
                      val description: String,
                      val forks_count: Int,
                      val stargazers_count: Int)

data class PullRequest(val id: Long,
                       val assignee: RelatedUser,
                       val url: String,
                       val state: String,
                       val title: String,
                       val body: String)