package com.bmx.desafioconcrete.data.room

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.bmx.desafioconcrete.util.SingletonHolder

/**
 * Created by thiagopassos on 12/12/2017.
 */
@Database(entities = arrayOf(User::class, Repository::class, PullRequest::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    companion object Instance : SingletonHolder<AppDatabase, Context>({
        Room.databaseBuilder(it.applicationContext, AppDatabase::class.java, "cache.db").build()
    })

    abstract fun gitHubDao() : GitHubDao
}