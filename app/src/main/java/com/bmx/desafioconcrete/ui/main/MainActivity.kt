package com.bmx.desafioconcrete.ui.main

import android.os.Bundle
import com.bmx.desafioconcrete.R
import com.bmx.desafioconcrete.base.BaseActivity
import com.bmx.desafioconcrete.data.room.AppDatabase

/**
 * Created by Burner on 10/12/2017.
 */
class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
    }
}