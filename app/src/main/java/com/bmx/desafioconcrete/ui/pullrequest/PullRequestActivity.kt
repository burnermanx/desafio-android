package com.bmx.desafioconcrete.ui.pullrequest

import android.os.Bundle
import com.bmx.desafioconcrete.R
import com.bmx.desafioconcrete.base.BaseActivity

/**
 * Created by Burner on 10/12/2017.
 */
class PullRequestActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository_detail)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }
}