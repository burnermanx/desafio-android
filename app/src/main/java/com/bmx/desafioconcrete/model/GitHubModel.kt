package com.bmx.desafioconcrete.model

import org.parceler.Parcel

/**
 * Created by thiagopassos on 12/12/2017.
 */
@Parcel(Parcel.Serialization.BEAN)
data class GitHubUser(
        var username: String = "",
        var name: String = "",
        var gravatarUrl: String = "")

@Parcel(Parcel.Serialization.BEAN)
data class GitHubRepository(
        var user: GitHubUser = GitHubUser(),
        var name: String = "",
        var fullName: String = "",
        var description: String = "",
        var forkCount: Int = 0,
        var starCount: Int = 0)

@Parcel(Parcel.Serialization.BEAN)
data class GitHubPullRequest(
        var user: GitHubUser = GitHubUser(),
        var name: String = "",
        var description: String = "",
        var state: String= "",
        var url: String = "")